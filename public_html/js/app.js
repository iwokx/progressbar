/* 
 Sam Saravillo
 */

var myArr = ""; 
var i; 
var z = 0;
var barCount=""; 
var bars = "";
var buttons = ""; 
var limit = "";
var barsVal = "";
var progressbar = ["progressbar1", "progressbar2", "progressbar3", "progressbar4", "progressbar5"];


function addSubtract(z) {
    var limit  = document.getElementById("limit").innerText;
    var limitNum = parseInt(limit);
    //console.log(limit);
    
        for (var i = 0; i < progressbar.length; i++) {
            var activeBar = document.getElementById(progressbar[i]);
            var aB = activeBar.classList.contains("active");
            var a = z;      
            if (aB === true ) { 
                activeBar.style.width = a;
                var wid = document.getElementById(progressbar[i]).style.width;
                var b = wid.slice(0, 2);

                a = parseInt(a);
                b = parseInt(b);

                var sumVal = a + b; 
                var total = sumVal+'%';

                if (sumVal <= limitNum ) {
                document.getElementById(progressbar[i]).style.width = total;
                document.getElementById(progressbar[i]).innerHTML = total;
                } else {
                    console.log("max");
                }

            } else { 
                console.log("false");
            }
        }
    }   



document.addEventListener("DOMContentLoaded", function(event) {    
            
    var xmlhttp = new XMLHttpRequest();
    var url = "http://pb-api.herokuapp.com/bars/";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                myArr = JSON.parse(this.responseText);
                getData(myArr);
                //console.log(myArr);
                //$("#progressbar1").css("width", "50%");
            }   
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();

    
        
    function getData() {
        limit = parseInt(myArr.limit); 
        document.getElementById("limit").innerHTML = limit;   
        
        while (z < myArr.bars.length) { 
            barCount += "<option id='progress"+(z + 1)+"' value='progress"+(z + 1)+"'>#progress" + (z + 1) + "</option>"; 
            z++;
        }
        document.getElementById("optionBar").innerHTML = barCount;

        for (i = 0; i < myArr.bars.length; i++) {
            //aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'
            //aria-valuenow='"+ parseInt(myArr.bars[i])+"' aria-valuemin='0' aria-valuemax='"+ parseInt(limit) +"' style='width:"+ parseInt(myArr.bars[i])+"%' 
            bars += "<div class='progress' id='progress-bar'><div id='progressbar"+(i + 1)+"' class='progress-bar progress-bar-striped progress-bar"+(i + 1)+"' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+ parseInt(myArr.bars[i])+"%'>" + parseInt(myArr.bars[i]) + "%</div></div>";
            }
            
            document.getElementById("outputJSONBar").innerHTML = bars;
            


        for (i = 0; i < myArr.buttons.length; i++) {
            buttons += "<div class='col-md-2'><button type='button' class='btn btn-primary btn-lg btn-block' id='"+myArr.buttons[i]+"' onclick='addSubtract("+myArr.buttons[i]+")'>" + myArr.buttons[i] + "</button></div>";
            }
            document.getElementById("outputJSONBtn").innerHTML = buttons;
                   
    }
    
    
    var selectItem = function() {
        var barSel1 = document.getElementById("optionBar");
        var selectedValue1 = barSel1.options[barSel1.selectedIndex].value;
        
        if (selectedValue1 === "progress1") {
            var newAttr = document.getElementById("progressbar1");
            newAttr.className += " active";
            document.getElementById("progressbar2").classList.remove("active");
            document.getElementById("progressbar3").classList.remove("active");
            document.getElementById("progressbar4").classList.remove("active");
            document.getElementById("progressbar5").classList.remove("active");

            console.log(newAttr); 
            } else if (selectedValue1 === "progress2") {
                var newAttr = document.getElementById("progressbar2");
                newAttr.className += " active";
                document.getElementById("progressbar1").classList.remove("active");
                document.getElementById("progressbar3").classList.remove("active");
                document.getElementById("progressbar4").classList.remove("active");
                document.getElementById("progressbar5").classList.remove("active");

                console.log(newAttr); 
                } else if (selectedValue1 === "progress3") {
                    var newAttr = document.getElementById("progressbar3");
                    newAttr.className += " active";
                    document.getElementById("progressbar1").classList.remove("active");
                    document.getElementById("progressbar2").classList.remove("active");
                    document.getElementById("progressbar4").classList.remove("active");
                    document.getElementById("progressbar5").classList.remove("active");

                    console.log(newAttr); 
                    }  else if (selectedValue1 === "progress4") {
                        var newAttr = document.getElementById("progressbar4");
                        newAttr.className += " active";
                        document.getElementById("progressbar1").classList.remove("active");
                        document.getElementById("progressbar2").classList.remove("active");
                        document.getElementById("progressbar3").classList.remove("active");
                        document.getElementById("progressbar5").classList.remove("active");

                        console.log(newAttr); 
                        }  else if (selectedValue1 === "progress5") {
                            var newAttr = document.getElementById("progressbar5");
                            newAttr.className += " active";
                            document.getElementById("progressbar1").classList.remove("active");
                            document.getElementById("progressbar2").classList.remove("active");
                            document.getElementById("progressbar3").classList.remove("active");
                            document.getElementById("progressbar4").classList.remove("active");

                            console.log(newAttr); 
                            } 

    };
    // select option 
    $('#optionBar').change(function() {
        selectItem($(this).val());
    });
    // selectitem
    selectItem($('#optionBar').find('option[selected]').val());

}); 
         

